define([
	'require',
	'madjoh_modules/styling/styling'
],
function(require, Styling){
	var Images = {
		init : function(ImageSettings){
			Images.sources = ImageSettings;

			var promises = [];
			for(var key in ImageSettings) promises.push(Images.setupImage(key));
			return Promise.all(promises);
		},

		setupImage : function(imageKey){
			var image = document.createElement('img');
				image.promise = {};
				image.promise.promise = new Promise(function(resolve, reject){
					image.promise.resolve 	= resolve;
					image.promise.reject 	= reject;
				});
				image.onload = function(){
					image.promise.resolve();
				};
				image.onerror = function(){
					console.log('Error loading image', image.src);
					image.promise.resolve();
				};

			image.src = 'app/images/' + Styling.getSizeCategory() + '/' + Images.sources[imageKey];
			return image.promise.promise;
		}
	};

	return Images;
});